promise实现原理
https://blog.csdn.net/qq_22167989/article/details/81586105
https://www.jianshu.com/p/43de678e918a
https://www.jianshu.com/p/b4f0425b22a1


es6的promise 采用了 Promise/A+ 规范。
promise总体来说实现比较简单，就是使用回调队列封装了回调调用方式。让callback hell转化成了同步的链式调用。
实现原理：
内部维持了一个state状态，指示该promise的状态(pending fulfilled rejected)
两个回调队列 一个保存fulfilled callback队列 一个保存rejected callback队列
这些回调队列由promise的then方法调用添加。

以下是一个简单的实现
function Promise(executor) {
    var self = this;
    self.status = 'pending'; //promise当前的状态
    self.data = undefined; //promise的值
    self.onResolvedCallback = [];
    //promise状态变为resolve时的回调函数集，可能有多个
    self.onRejectedCallback = [];
    //promise状态变为reject时的回调函数集，可能有多个
   function resolve(value) {
       if(self.status === 'pending') {
           self.status = 'resolved';
           self.data = value;
           for(var i = 0; i < self.onResolvedCallback.length; i++) {
               self.onResolvedCallback[i](value);
           }
       }
   }

   function reject(reason) {
        if(self.status === 'pending') {
            self.status = 'rejected';
            self.data = reason;
            for(var i = 0; i < self.onRejectedCallback.length; i++) {
                self.onRejectedCallback[i](reason);
            }
        }
   }

   try {
       executor(resolve, reject);
   } catch (e){
       reject(e);
   }
};
Promise.prototype.then = function (onResolve, onReject) {
    this.onResolvedCallback.push(onResolve);
    this.onRejectedCallback.push(onReject);
};

但是这里有个问题 如果executor中调用同步函数
const promise = new Promise((resolve) => {
    resolve(1);
});
promise.then((a) => alert(a));
那么该promise在被创建之后立马就失效，都等不到then去放置回调函数了。
由于我们并不能够控制用户在executor里面调用什么样的函数，这里需要resolve和reject自身去支持调用同步接口。
将resolve和reject进行如下改写：
function resolve(value) {
    setTimeout(function () {
        if(self.status === 'pending') {
            self.status = 'resolved';
            self.data = value;
            for(var i = 0; i < self.onResolvedCallback.length; i++) {
                self.onResolvedCallback[i](value);
            }
        }
    })
}

function reject(reason) {
    setTimeout(function () {
        if(self.status === 'pending') {
            self.status = 'rejected';
            self.data = reason;
            for(var i = 0; i < self.onRejectedCallback.length; i++) {
                self.onRejectedCallback[i](reason);
            }
        }
    })
}


在构造函数中的示例中，then方法并没有返回一个promise对象，而 Promise/A+ 规范中规定then方法用来注册promise对象状态改变时的回调，且返回一个新的promise对象。
需要做如下改写
Promise.prototype.then = function (onResolved, onRejected) {
    var self = this;
    var promise2;
    onResolved = typeof onResolved === 'function'
                ? onResolved
                : function (value) {return value};
    onRejected = typeof onRejected === 'function'
                ? onRejected
                : function (reason) {throw reason};
    //promise对象当前状态为resolved
    if(self.status === 'resolved') {
        return promise2 = new Promise(function (resolve, reject) {
            try {
                //调用onResolve回调函数
                var x = onResolved(self.data);
                //如果onResolve回调函数返回值为一个promise对象
                if(x instanceof  Promise) {
                    //将它的结果作为promise2的结果
                    x.then(resolve, reject);
                } else {
                    resolve(x);//执行promise2的onResolve回调
           }
            } catch (e) {
                reject(e); //执行promise2的onReject回调
            }
        })
    }
    //promise对象当前状态为rejected
    if(self.status === 'rejected') {
        return promise2 = new Promise(function (resolve, reject) {
            try {
                var x = onRejected(self.data);
                if (x instanceof Promise) {
                    x.then(resolve, reject)
                } else {
                    resolve(x)
                }
            } catch (e) {
                reject(e)
            }
        })
    }
    //promise对象当前状态为pending
    //此时并不能确定调用onResolved还是onRejected，需要等当前Promise状态确定。
    //所以需要将callBack放入promise1的回调数组中
    if(self.status === 'pending') {
        return promise2 = new Promise(function (resolve, reject) {
            self.onResolvedCallback.push(function (value) {
                try {
                    var x = onResolved(self.data);
                    if (x instanceof Promise) {
                        x.then(resolve, reject);
                    } else {
                        resolve(x);
          }
                } catch (e) {
                    reject(e);
                }
            })
            self.onRejectedCallback.push(function(reason) {
                try {
                    var x = onRejected(self.data);
                    if (x instanceof Promise) {
                        x.then(resolve, reject)
                    } else {
                        resolve(x);
                    }
                } catch (e) {
                    reject(e)
                }
            })
        })
    }
};
