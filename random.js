function chooseByWeightWithIndex(elements, fn) {
    var weights = elements.map(fn);
    var total = weights.reduce(function(a, b) {return a + b;}, 0);
    var r = Math.random() * total;
    var len = elements.length;
    var acc = 0;
    for (var i = 0; i < len; i++) {
        acc += weights[i];
        if (r < acc) return {element: elements[i], index: i};
    }
    return null;
}

console.log(JSON.stringify(chooseByWeightWithIndex([{x:0,w:0},{x:1,w:0},{x:2,w:0},{x:3,w:0},{x:4,w:0},{x:5,w:0}],function(e){return e.w;})));
console.log(JSON.stringify(chooseByWeightWithIndex([{x:0,w:0},{x:1,w:1},{x:2,w:2},{x:3,w:0},{x:4,w:0},{x:5,w:3}],function(e){return e.w;})));
console.log(JSON.stringify(chooseByWeightWithIndex([{x:0,w:1},{x:1,w:2},{x:2,w:3},{x:3,w:4},{x:4,w:5},{x:5,w:6}],function(e){return e.w;})));
console.log(JSON.stringify(chooseByWeightWithIndex([{x:0,w:1},{x:1,w:0},{x:2,w:0},{x:3,w:0},{x:4,w:0},{x:5,w:4}],function(e){return e.w;})));